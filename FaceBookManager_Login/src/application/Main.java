package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import application.encryption.AES;
import application.encryption.Encryption;
import application.updater.Downloader;
import application.updater.UFile;
import controller.GUIController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;

public class Main extends Application {
	static GUIController ctrl;
	static String[] atributes = {};
	private static UFile localUpdate;
	private static boolean localFileLoaded = false;
	private static UFile remoteUpdate;
	private static boolean remoteFileLoaded = false;
		
	@Override
	public void start(Stage primaryStage) {

		try {
			FXMLLoader fxml = new FXMLLoader(getClass().getResource("/view/GUI.fxml"));
			AnchorPane layout = (AnchorPane) fxml.load();
			ctrl = fxml.getController();
			ctrl.setMain(this);
			Scene scene = new Scene(layout);
			primaryStage.setTitle("Social Marketter Pro v_1.0");
			primaryStage.setScene(scene);
			primaryStage.getIcons().add(new Image("icon.png"));
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {

		atributes = args;
		checkForUpdate();
		File file = new File("FBapp.jar");
		if (!file.exists()) {
			downloadApp();
		}
		launch(args);

	}

	public void login(String username, String password) {
		System.out.println("Working_OK");
		try {			
			Runtime.getRuntime().exec("java -jar FBapp.jar " + username + " " + password);
		} catch (IOException e) {
			System.out.println("File not found");
		}
	}

	public static String[] getAtributes() {
		return atributes;
	}

	private static boolean checkForUpdate() {
		boolean status = false;
		File details = new File("Update/update.txt");
		if (details.exists()) {
			details.delete();
		}
		
		status = downloadUpdateInfo();
		
		File fileL = new File("version.txt");
		File fileR = new File("Update/update.txt");
		if (loadFile(Type.LOCAL, fileL)) {
			localFileLoaded = true;
		}
		if (loadFile(Type.REMOTE, fileR)) {
			remoteFileLoaded = true;
		}
		if (localFileLoaded && remoteFileLoaded) {
			if (!localUpdate.getSha().equals(remoteUpdate.getSha())) {
				File file = new File("FBapp.jar");
				if (file.exists()) {
					file.delete();
				}
				status = downloadApp();
			}
		}
		if (!localFileLoaded) {
			File file = new File("FBapp.jar");
			if (file.exists()) {
				file.delete();
			}
			status = downloadApp();
		}

		return status;

	}

	private static boolean loadFile(Type Type, File file) {
		if (!file.exists()) {
			return false;
		}
		boolean status = true;
		String line = "";
		String[] data = { "", "", "", "" };
		UFile uFile = new UFile();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {

				data = line.split(";");
				System.out.println(line);
			}

			uFile.setName(data[0].replaceFirst("NAME=", ""));
			uFile.setVersion(data[1].replaceFirst("VERSION=", ""));
			uFile.setLocation(data[2].replaceFirst("LOCATION=", ""));
			uFile.setSha(data[3].replaceFirst("SHA-1=", ""));
			switch (Type) {
			case LOCAL:
				localUpdate = uFile;
				break;
			case REMOTE:
				remoteUpdate = uFile;
				break;
			}

			br.close();
		} catch (FileNotFoundException e) {
			status = false;
		} catch (IOException e) {
			status = false;
		}

		return status;
	}

	private enum Type {
		LOCAL, REMOTE
	}

	public static void saveLoginData(String username, String password) {
		File file = new File("data.dat");

		try {
			FileWriter writer = new FileWriter(file);
			String encrypted = AES.encrypt(username + ":" + password, Encryption.CODE_0);
			writer.write(encrypted);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String loadLoginData() {
		String data = null;
		File file = new File("data.dat");
		if (!file.exists()) {
			return null;
		}
		String line = "";

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				data = AES.decrypt(line, Encryption.CODE_0);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return data;
	}

	private static boolean downloadApp() {
		boolean status = true;
		Downloader down = new Downloader();
		try {
			down.download(new URL(remoteUpdate.getLocation()), new File("FBapp.jar"));
			down.download(new URL("http://94.177.172.206/Update/update.txt"), new File("version.txt"));
			System.out.println("Remote version is different - downloading");
		} catch (MalformedURLException e) {
			status = false;
		}
		return status;
	}

	private static boolean downloadUpdateInfo() {
		boolean status = true;
		Downloader down = new Downloader();
		try {
			down.download(new URL("http://94.177.172.206/Update/update.txt"), new File("Update/update.txt"));
		} catch (MalformedURLException e) {
			status = false;
		}
		return status;

	}
}
