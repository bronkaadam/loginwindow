package controller;

import java.util.EventListener;
import application.Main;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class GUIController {

	@FXML
	private PasswordField txPassword;

	@FXML
	private Button btLogin;

	@FXML
	private TextField txUsername;

	@FXML
	private CheckBox cbSaveLoginData;

	private String username;
	private String password;
	
	
	Main mainApp;
	boolean clicked = false;
	String[] atributes = {};

	public void setMain(Main main) {
		mainApp = main;
	}

	@FXML
	void initialize() {
		atributes = Main.getAtributes();
		if (atributes.length != 0) {
			if (atributes[0].equals("WRONG_PASSWORD")) {
				txUsername.setText(atributes[1]);
				txUsername.setFocusTraversable(false);
				txPassword.setStyle("-fx-background-color: red");
				txPassword.requestFocus();

			}
			if (atributes[0].equals("WRONG_USERNAME_AND_PASSWORD")) {
				txUsername.setStyle("-fx-background-color: red");
				txPassword.setStyle("-fx-background-color: red");

			}
		}
		btLogin.armedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				String username = txUsername.getText();
				String password = txPassword.getText();

				if (!username.isEmpty() && !password.isEmpty()) {
					if (!clicked) {
						mainApp.login(username, password);
						btLogin.setDisable(true);
						txUsername.setEditable(false);
						txPassword.setEditable(false);
						if (cbSaveLoginData.isSelected()) {
							Main.saveLoginData(username, password);
						}
						clicked = true;
					}
					Task<Void> sleeper = new Task<Void>() {

						@Override
						protected Void call() throws Exception {
							Thread.sleep(2000);
							return null;
						}

					};
					sleeper.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

						@Override
						public void handle(WorkerStateEvent event) {
							System.exit(0);

						}

					});
					new Thread(sleeper).start();
				}

			}

		});
		txPassword.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent arg0) {
				if (arg0.getCode() == KeyCode.ENTER) {
					String username = txUsername.getText();
					String password = txPassword.getText();

					if (!username.isEmpty() && !password.isEmpty()) {
						if (!clicked) {
							mainApp.login(username, password);
							btLogin.setDisable(true);
							txUsername.setEditable(false);
							txPassword.setEditable(false);
							if (cbSaveLoginData.isSelected()) {
								Main.saveLoginData(username, password);
							}
							clicked = true;
						}
						Task<Void> sleeper = new Task<Void>() {

							@Override
							protected Void call() throws Exception {
								Thread.sleep(2000);
								return null;
							}

						};
						sleeper.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

							@Override
							public void handle(WorkerStateEvent event) {
								System.exit(0);

							}

						});
						new Thread(sleeper).start();
					}

				}
			}

		});
		if (getLoginData(Main.loadLoginData())) {
			txUsername.setText(username);
			txPassword.setText(password);
						
		}
	}

	private boolean getLoginData(String data) {
		boolean status = true;
		if (data != null) {
			String[] c = data.split(":");
			username = c[0];
			password = c[1];
			
		} else {
			status = false;
		}
		return status;
	}

}
